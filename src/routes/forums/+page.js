import { supabase } from '$lib/supabaseClient';
import { error } from '@sveltejs/kit';

/** @type {import('./$types').PageLoad} */
export async function load() {
	let { data, error: err } = await supabase
		.from('forum')
		.select('*')
		.order('votes', { ascending: false });
	if (err) {
		throw error(500, err);
	}
	return {
		forums: data
	};
}
