import { supabase } from '$lib/supabaseClient';
import { invalid } from '@sveltejs/kit';

/** @type {import('./$types').Actions} */
export const actions = {
	postComments: async ({ request }) => {
		const data = await request.formData();
		const posted_by = data.get('posted_by');
		const title = data.get('title');
		const detail = data.get('detail');
		const { error } = await supabase.from('comments').insert([{ title, detail }]);

		if (error) {
			return invalid(500, error);
		}
	}
};